import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ConfigurationsScreen extends StatefulWidget {
  ConfigurationsScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _PageState createState() => _PageState();
}

class _PageState extends State<ConfigurationsScreen> {

  var bip, copy, open;

  @override
  void initState() {
    bip = true;
    copy = true;
    open = true;
    super.initState();
    getConfigData();
  }

  @override
  void dispose() {
    super.dispose();
    setConfigData();
  }


  getConfigData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _bip = prefs.getBool('bip') == null ? true : prefs.getBool('bip');
    bool _copy = prefs.getBool('copy') == null ? true : prefs.getBool('copy');
    bool _open = prefs.getBool('open') == null ? true : prefs.getBool('open');
    setState(() {
      bip =_bip;
      copy =_copy;
      open =_open;
    });
  }

  setConfigData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('bip', bip);
    await prefs.setBool('copy', copy);
    await prefs.setBool('open', open);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text('Configuración'),
        leading: IconButton(
          icon: Icon(Icons.menu),
          onPressed: () {
            RootScaffold.openDrawer(context);
          },
        ),
      ),
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 30.0, right: 30.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Bip',
                      style: TextStyle(
                        fontSize: 16.0
                      ),
                    ),
                    Checkbox(
                      value: bip,
                      onChanged: (bool newValue) {
                        setState(() { bip = newValue; });
                      }
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 30.0, right: 30.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Copiar al portapapeles',
                      style: TextStyle(
                        fontSize: 16.0
                      ),
                    ),
                    Checkbox(
                      value: copy,
                      onChanged: (bool newValue) {
                        setState(() { copy = newValue; });
                      }
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 30.0, right: 30.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Abrir páginas web automáticamente',
                      style: TextStyle(
                        fontSize: 16.0
                      ),
                    ),
                    Checkbox(
                      value: open,
                      onChanged: (bool newValue) {
                        setState(() { open = newValue; });
                      }
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      )
    );
  }
}

class RootScaffold {
  static openDrawer(BuildContext context) {
    final ScaffoldState scaffoldState =
        context.rootAncestorStateOfType(TypeMatcher<ScaffoldState>());
    scaffoldState.openDrawer();
  }

  static ScaffoldState of(BuildContext context) {
    final ScaffoldState scaffoldState =
        context.rootAncestorStateOfType(TypeMatcher<ScaffoldState>());
    return scaffoldState;
  }
}