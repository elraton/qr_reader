import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show TargetPlatform;
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';


class CustomDrawer extends StatelessWidget {
  final GlobalKey<NavigatorState> navigator;

  const CustomDrawer({Key key, this.navigator}) : super(key: key);

  _launchUrl(value) async {
    if (await canLaunch(value)) {
      await launch(value);
    } else { }
  }

  @override
  Widget build(BuildContext context) {
    final state = RootDrawer.of(context);
    return Container(
          decoration: BoxDecoration(
            color: Colors.white
          ),
          child: ListView(
            padding: EdgeInsets.only(top: 30.0, bottom: 0.0, left: 0.0, right: 0.0),
            children: <Widget>[
              ListTile(
                selected: true,
                contentPadding: EdgeInsets.all(0.0),
                title: Container(
                  padding: EdgeInsets.only(bottom: 10.0, left: 15.0, right: 15.0),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        width: 1.0,
                        color: Color(0xFFEAEAEA)
                      )
                    )
                  ),
                  child: Row(
                    children: <Widget>[
                      Image(
                        image: AssetImage('assets/images/scan_icon.jpeg'),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 30.0),
                        child: Text(
                          'Escanear',
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Color(0xFF878787)
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                onTap: () { 
                  navigator.currentState.pushReplacementNamed("/");
                  state.close(); 
                },
              ),
              ListTile(
                contentPadding: EdgeInsets.all(0.0),
                title: Container(
                  padding: EdgeInsets.only(bottom: 10.0, left: 15.0, right: 15.0),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        width: 1.0,
                        color: Color(0xFFEAEAEA)
                      )
                    )
                  ),
                  child: Row(
                    children: <Widget>[
                      Image(
                        image: AssetImage('assets/images/historial_icon.jpeg'),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 30.0),
                        child: Text(
                          'Historial',
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Color(0xFF878787)
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                onTap: () { 
                  navigator.currentState.pushReplacementNamed("/history");
                  state.close();
                },
              ),
              ListTile(
                contentPadding: EdgeInsets.all(0.0),
                title: Container(
                  padding: EdgeInsets.only(bottom: 10.0, left: 15.0, right: 15.0),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        width: 1.0,
                        color: Color(0xFFEAEAEA)
                      )
                    )
                  ),
                  child: Row(
                    children: <Widget>[
                      Image(
                        image: AssetImage('assets/images/config_icon.jpeg'),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 30.0),
                        child: Text(
                          'Configuración',
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Color(0xFF878787)
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                onTap: () { 
                  // navigator.currentState.pushNamed("/config");
                  navigator.currentState.pushReplacementNamed("/config");
                  state.close();
                },
              ),
              ListTile(
                contentPadding: EdgeInsets.all(0.0),
                title: Container(
                  padding: EdgeInsets.only(bottom: 10.0, left: 15.0, right: 15.0),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        width: 1.0,
                        color: Color(0xFFEAEAEA)
                      )
                    )
                  ),
                  child: Row(
                    children: <Widget>[
                      Image(
                        image: AssetImage('assets/images/share_icon.jpeg'),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 30.0),
                        child: Text(
                          'Compartir',
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Color(0xFF878787)
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                onTap: () {
                  if(Theme.of(context).platform == TargetPlatform.android) {
                    // Share.share('Tienes que probar esta app ' + 'https://play.google.com/store/apps/details?id=com.gamma.scan');
                    Share.share('Hola estoy usando este lector de códigos QR, súper rápido y SIN PUBLICIDAD, ¡Pruébalo ahora! ' + 'http://www.qrperu.org');
                  }
                  if(Theme.of(context).platform == TargetPlatform.iOS) {
                    Share.share('Aqui va la url del App Store para compartir la APP');
                  }
                  if(Theme.of(context).platform == TargetPlatform.fuchsia) {
                    Share.share('Aqui va la url del Market para compartir la APP');
                  }
                },
              ),
              ListTile(
                contentPadding: EdgeInsets.all(0.0),
                title: Container(
                  padding: EdgeInsets.only(bottom: 10.0, left: 15.0, right: 15.0),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        width: 1.0,
                        color: Color(0xFFEAEAEA)
                      )
                    )
                  ),
                  child: Row(
                    children: <Widget>[
                      Image(
                        image: AssetImage('assets/images/like_icon.jpeg'),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 30.0),
                        child: Text(
                          'Me gusta',
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Color(0xFF878787)
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                onTap: () {
                  _launchUrl('http://www.qrperu.org');
                },
              ),
              ListTile(
                contentPadding: EdgeInsets.all(0.0),
                title: Container(
                  padding: EdgeInsets.only(bottom: 10.0, left: 15.0, right: 15.0),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        width: 1.0,
                        color: Color(0xFFEAEAEA)
                      )
                    )
                  ),
                  child: Row(
                    children: <Widget>[
                      Image(
                        image: AssetImage('assets/images/vl.png'),
                        height: 30.0,
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 30.0),
                        child: Text(
                          'Nuestras Apps',
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Color(0xFF878787)
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                onTap: () {
                  if(Theme.of(context).platform == TargetPlatform.android) {
                    // _launchUrl('https://play.google.com/store/apps/dev?id=7739323866949324662');
                  }
                  if(Theme.of(context).platform == TargetPlatform.iOS) {
                    // _launchUrl('Url para el App Store');
                  }
                  if(Theme.of(context).platform == TargetPlatform.fuchsia) {
                    // _launchUrl('Url del market de fuchsia');
                  }
                },
              ),
            ],
          ),
        );
  }
}

class RootDrawer {
  static DrawerControllerState of(BuildContext context) {
    final DrawerControllerState drawerControllerState =
        context.rootAncestorStateOfType(TypeMatcher<DrawerControllerState>());
    return drawerControllerState;
  }
}