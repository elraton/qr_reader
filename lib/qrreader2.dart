import 'package:flutter/material.dart';
// import 'package:qr_mobile_vision/qr_camera.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:clipboard_manager/clipboard_manager.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:async';
// import 'dart:convert';
import 'package:fast_qr_reader_view/fast_qr_reader_view.dart';
import 'package:flutter_lantern/flutter_lantern.dart';

class QrReaderScreen2 extends StatefulWidget {
  QrReaderScreen2({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _PageState createState() => _PageState();
}

class _PageState extends State<QrReaderScreen2> {
  static AudioCache player = new AudioCache();
  var lastcode;
  bool lights = false;
  bool bip, copy, open;

  QRReaderController controller;
  List<CameraDescription> cameras;

  final GlobalKey<ScaffoldState> mScaffoldState = new GlobalKey<ScaffoldState>();

  Future<Null> getcameras() async {
    cameras = await availableCameras();
    onNewCameraSelected(cameras[0]);
  }

  @override
  void initState() {
    lastcode = '';
    lights = false;
    super.initState();

    getcameras();
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  getConfigData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _bip = prefs.getBool('bip') == null ? true : prefs.getBool('bip');
    bool _copy = prefs.getBool('copy') == null ? true : prefs.getBool('copy');
    bool _open = prefs.getBool('open') == null ? true : prefs.getBool('open');
    setState(() {
      bip =_bip;
      copy =_copy;
      open =_open;
    });
  }

  addtoHistory(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> history = prefs.getStringList('history');
    if (history == null ) {
      history = new List<String>();
      history.add(value);
    } else {
      if (history.length < 50) {
        history.add(value);
      } else {
        history.removeAt(0);
        history.add(value);
      }
    }
    await prefs.setStringList('history', history);
  }

  switchlights() async {
    setState(() {
      lights = !lights;
    });
    controller.toggleFlash();
  }

  getImage() {
    if (lights == true) {
      return Image(image: AssetImage('assets/images/light_on.png'),
                height: 30.0,
              );
    } else {
      return Image(image: AssetImage('assets/images/light_off.png'),
                height: 30.0,
              );
    }
  }

  void showInSnackBar(String message) {
    /*final snackBar = SnackBar(
      content: Text(message),
    );*/
    final snackBar = new SnackBar(content: new Text(message));
    mScaffoldState.currentState.showSnackBar(snackBar);
    // Scaffold.of(context).showSnackBar(snackBar);
  }

  void onCodeRead(dynamic value) async {
    print('ssss'); 
    if (bip) {
      const bip_sound = "sounds/bip.mp3";
      player.play(bip_sound);
    }

    if (copy) {
      if (lastcode != value) {
        Clipboard.setData(new ClipboardData(text: value));
        /*ClipboardManager.copyToClipBoard(value).then((result) {
          showInSnackBar('Se copio al portapapeles');
        });*/
      }
    }

    if (open) {
      if (await canLaunch(value)) {
        launch(value);
      } else {
        if (await canLaunch('http://' + value)) {
          launch('http://' + value);
        } else {
          showInSnackBar('No se puede abrir la url');
        }
      }
    }

    addtoHistory(value);
    
    setState(() {
      lastcode = value;
    });


    // showInSnackBar(value.toString());
    new Future.delayed(const Duration(seconds: 5), controller.startScanning);
    // getcameras();
  }

  void onNewCameraSelected(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller.dispose();
    }
    controller = new QRReaderController(cameraDescription, ResolutionPreset.high,
        [CodeFormat.qr], onCodeRead);

    // If the controller is updated then update the UI.
    controller.addListener(() {
      if (mounted) setState(() {});
      if (controller.value.hasError) {
        showInSnackBar('Camera error ${controller.value.errorDescription}');
      }
    });

    try {
      await controller.initialize();
    } on QRReaderException catch (e) {
      logError(e.code, e.description);
      showInSnackBar('Error: ${e.code}\n${e.description}');
    }

    if (mounted) {
      setState(() {});
      controller.startScanning();
    }
  }

  Widget _cameraPreviewWidget() {
    if (controller == null || !controller.value.isInitialized) {
      return const Text(
        'No camera selected',
        style: const TextStyle(
          color: Colors.white,
          fontSize: 24.0,
          fontWeight: FontWeight.w900,
        ),
      );
    } else {
      return new AspectRatio(
        aspectRatio: controller.value.aspectRatio,
        child: new QRReaderPreview(controller),
      );
    }
  }
  
  void logError(String code, String message) =>
    print('Error: $code\nError Message: $message');

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width - 90.0;
    getConfigData();
    
    if (cameras != null) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF211f20),
          title: Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Escanea el QR'),
                IconButton(
                  icon: getImage(),
                  onPressed: () { switchlights(); },
                ),
              ],
            ),
          ),
          leading: IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {
              RootScaffold.openDrawer(context);
            },
          ),
        ),
        body: Container(
          decoration: BoxDecoration(
            color: Color(0xFF000000)
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 0.0, top: 0.0),
                  child: Image(
                    image: NetworkImage('http://www.qrperu.org/lu.jpg'),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 1.0,
                      color: Color(0xFF717171),
                    ),
                  ),
                  width: width,
                  height: width,
                  child: _cameraPreviewWidget()  
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 0.0, top: 0.0),
                  child: Image(
                    image: NetworkImage('http://www.qrperu.org/ld.jpg'),
                  ),
                ),
              ],
            ),
          ),
        )
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF211f20),
          title: Text('Escanea el QR'),
          leading: IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {
              RootScaffold.openDrawer(context);
            },
          ),
        ),
        body: Container(
          decoration: BoxDecoration(
            color: Color(0xFF211f20)
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                CircularProgressIndicator(),
                Text('Cargando')
              ],
            ),
          ),
        )
      );
    }
  }
}

class RootScaffold {
  static openDrawer(BuildContext context) {
    final ScaffoldState scaffoldState =
        context.rootAncestorStateOfType(TypeMatcher<ScaffoldState>());
    scaffoldState.openDrawer();
  }

  static ScaffoldState of(BuildContext context) {
    final ScaffoldState scaffoldState =
        context.rootAncestorStateOfType(TypeMatcher<ScaffoldState>());
    return scaffoldState;
  }
}
