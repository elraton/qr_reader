import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class HistoryScreen extends StatefulWidget {
  HistoryScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _PageState createState() => _PageState();
}

class _PageState extends State<HistoryScreen> {

  List<String> historylist;

  @override
  void initState() {
    super.initState();
  }

  getHistorygData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> _historylist = prefs.getStringList('history');
    setState(() {
      historylist = _historylist;
    });
  }

  openUrl(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      if (await canLaunch('http://' + url)) {
        await launch('http://' + url);
      } else {
        showInSnackBar('No se puede abrir la url');
      }
    }
  }

  void showInSnackBar(String message) {
    final snackBar = SnackBar(
      content: Text(message),
    );
    Scaffold.of(context).showSnackBar(snackBar);
  }

  loop(position) {
    String item = historylist.elementAt(position);
    
    return Container(
      margin: EdgeInsets.only(bottom: 15.0),
      padding: EdgeInsets.only(left: 30.0, right: 30.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            (position + 1 ).toString(),
            style: TextStyle(
              fontSize: 16.0
            ),
          ),
          InkWell(
            child: Text(
              item.length > 40 ? item.substring(0, 40) + '...' : item,
              style: TextStyle(
                fontSize: 16.0
              ),
            ),
            onTap: () { openUrl(item); },
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    getHistorygData();

    if (historylist != null) {
      if (historylist.length > 0) {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.red,
            title: Text('Historial'),
            leading: IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {
                RootScaffold.openDrawer(context);
              },
            ),
          ),
          body: Container(
            padding: EdgeInsets.only(top: 30.0),
            child: Center(
              child: ListView.builder(
                itemCount: historylist.length,
                itemBuilder: (context, position) {
                  return loop(position);
                },
              )
            ),
          )
        );      
      } else {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.red,
            title: Text('Historial'),
            leading: IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {
                RootScaffold.openDrawer(context);
              },
            ),
          ),
          body: Container(
            padding: EdgeInsets.only(top: 30.0),
            child: Center(
              child: Text('No hay elementos que mostrar')
            ),
          )
        );
      }
    } else {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red,
          title: Text('Historial'),
          leading: IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {
              RootScaffold.openDrawer(context);
            },
          ),
        ),
        body: Container(
          padding: EdgeInsets.only(top: 30.0),
          child: Center(
            child: Text('No hay elementos que mostrar')
          ),
        )
      );
    }
  }
}

class RootScaffold {
  static openDrawer(BuildContext context) {
    final ScaffoldState scaffoldState =
        context.rootAncestorStateOfType(TypeMatcher<ScaffoldState>());
    scaffoldState.openDrawer();
  }

  static ScaffoldState of(BuildContext context) {
    final ScaffoldState scaffoldState =
        context.rootAncestorStateOfType(TypeMatcher<ScaffoldState>());
    return scaffoldState;
  }
}