import 'package:flutter/material.dart';
// import 'dart:async';
// import 'package:fast_qr_reader_view/fast_qr_reader_view.dart';
import './configurations.dart';
import './qrreader.dart';
import './qrreader2.dart';
import './drawer.dart';
import './history.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => QrReaderScreen2(),
        '/config': (context) => ConfigurationsScreen(),
        '/history': (context) => HistoryScreen(),
      },
      builder: (context, child) {
        return Scaffold(
          drawer: CustomDrawer(navigator: (child.key as GlobalKey<NavigatorState>)),
          body: child,
        );
      },
    );
  }
}