import 'package:flutter/material.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:fast_qr_reader_view/fast_qr_reader_view.dart';

class QrReaderScreen extends StatefulWidget {
  QrReaderScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _PageState createState() => _PageState();
}

class _PageState extends State<QrReaderScreen> {
  static AudioCache player = new AudioCache();
  var lastcode;
  bool bip, copy, open;
  String barcode = "";
  List<CameraDescription> cameras;
  QRReaderController controller;

  @override
  void initState() {
    lastcode = '';
    super.initState();
    controller = new QRReaderController(cameras[0], ResolutionPreset.high, [CodeFormat.qr], (dynamic value){
      print(value); // the result!
      // ... do something
      // wait 3 seconds then start scanning again.
      new Future.delayed(const Duration(seconds: 3), controller.startScanning);
    });
    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
      controller.startScanning();
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
  
  foundQR(code) async {
    
    if (bip) {
      const bip_sound = "sounds/bip.mp3";
      player.play(bip_sound);
    }

    /*if (copy) {
      if (lastcode != code) {
        ClipboardManager.copyToClipBoard(code).then((result) {
          final snackBar = SnackBar(
            content: Text('Se copió al portapapeles'),
          );
          Scaffold.of(context).showSnackBar(snackBar);
        });
      }
    }*/

    if (open) {
      if (await canLaunch(code)) {
        await launch(code);
      } else {
        final snackBar = SnackBar(
          content: Text('No se puede abrir la url'),
        );
        Scaffold.of(context).showSnackBar(snackBar);        
      }
    }
    
    setState(() {
      lastcode = code;
    });
  }

  getConfigData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _bip = prefs.getBool('bip') == null ? true : prefs.getBool('bip');
    bool _copy = prefs.getBool('copy') == null ? true : prefs.getBool('copy');
    bool _open = prefs.getBool('open') == null ? true : prefs.getBool('open');
    setState(() {
      bip =_bip;
      copy =_copy;
      open =_open;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width - 30.0;
    getConfigData();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF211f20),
        title: Text('Escanea el QR'),
        leading: IconButton(
          icon: Icon(Icons.menu),
          onPressed: () {
            RootScaffold.openDrawer(context);
          },
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
          color: Color(0xFF211f20)
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(bottom: 30.0, top: 50.0),
                child: Image(
                  image: AssetImage('assets/images/logo.jpeg'),
                  width: 200.0,
                  height: 50.0,
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 1.0,
                    color: Color(0xFF717171),
                  ),
                ),
                constraints: BoxConstraints.loose ( Size(width, width) ),
                child: new QRReaderPreview(controller)
              ),
            ],
          ),
        ),
      )
    );
  }
}



class RootScaffold {
  static openDrawer(BuildContext context) {
    final ScaffoldState scaffoldState =
        context.rootAncestorStateOfType(TypeMatcher<ScaffoldState>());
    scaffoldState.openDrawer();
  }

  static ScaffoldState of(BuildContext context) {
    final ScaffoldState scaffoldState =
        context.rootAncestorStateOfType(TypeMatcher<ScaffoldState>());
    return scaffoldState;
  }
}
